1. Download latest version of python. www.python.org/downloads

2. Install python. Select pip on install, install for all users, add python to environment variables, create shortcuts.

3. Verify windows is installed. Open Powershell in "run as administrator" mode and type "python -v"

4. Verify pip installed by typing "pip freeze"

5. Update powershell settings - type command Set-ExecutionPolicy Unrestricted

6. Create "dev" folder with command "cd ~/" and then "mkdir Dev"

7. Install a virtual environment - pip install virtual env

8. Verify the install - pip freeze

9. Navigate to Dev folder in powershell - cd ~/Dev

10. Make a venv parent directory - mkdir home

11. Navigate to directory - cd home

12. Create Venv - virtualenv .

13. SHORTCUT
cd~/Dev
virtualenv venv
cd venv

14. Activate Virtual environment
cd ~\Dev\home
.\Scripts\activate
practice activating venv and deactivating.
deactivate

15. Verify. pip freeze

16. Install Django.
pip install django==2.0.7

17. Navigate to folder with "manage.py" file. - cd src

18. Start django server - python manage.py runserver

19. Go to server URL - 127.0.0.1:8000





.\Scripts\activate

